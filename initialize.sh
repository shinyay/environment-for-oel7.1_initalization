#!/bin/sh
yum install -y vim
git config --global user.name "Shinya Yanagihara"
git config --global user.email "shinya.com@gmail.com"
git config --global core.editor 'vim -c "set fenc=utf-8"'
git config --global color.diff auto
git config --global color.status auto
git config --global color.branch auto

#wget https://opscode-omnibus-packages.s3.amazonaws.com/el/7/x86_64/chefdk-0.9.0-1.el7.x86_64.rpm
#wget https://opscode-omnibus-packages.s3.amazonaws.com/el/7/x86_64/chefdk-0.10.0-1.el7.x86_64.rpm
wget https://packages.chef.io/stable/el/7/chefdk-0.14.25-1.el7.x86_64.rpm
rpm -ivh chefdk-0.14.25-1.el7.x86_64.rpm

echo 'eval "$(chef shell-init bash)"' >> ~/.bash_profile

echo 'export EDITOR=vim' >> ~/.bashrc
